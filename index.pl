#!/usr/bin/env perl

=pod

=encoding UTF-8

=head1 NAME

LoveCrypt
index.pl

=head1 VERSION

0.01

=head1 DESCRIPTION

In 2006, Russian company SUP bought the LiveJournal, ruining the original engine
 by adding a police tracking system and tons of advertising.

In 2014, they've closed the official LiveJournal code repository.

LoveCrypt is fork of LiveJournal before its acquisition by SUP.

We retrieved the source code from Wayback Machive targeted at 2003:

https://web.archive.org/web/20070214155614/http://www.livejournal.org:80/download/code/livejournal-2003082500.tar.gz

No ads, no police tracking and other shit.

=head1 COPYRIGHT AND LICENSE

Copyright (C) 1994-2003 LiveJournal.com, Inc., a subsidiary of Six Apart, Ltd.
Copyright (C) 2019-2021 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
                                        <neva_blyad@lovecri.es>

This file is part of LoveCrypt.

LoveCrypt is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LoveCrypt is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LoveCrypt.  If not, see <https://www.gnu.org/licenses/>.

To see a copy of the license and information with full list of Non-GPL
components distributed by us please visit:
www.lovecrypt7k5p7uh.onion/copying/

=head1 AUTHORS

=over

=item Brad Fitzpatrick <bradfitz@bradfitz.com>

=item LiveJournal.com, Inc.

=item НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
                      <neva_blyad@lovecri.es>

=item Invisible Light

=back

=cut

# We are serious about Perl
use strict;
use warnings;

# Some I/O functions should fuck up everything
use autodie 'chdir';

# CPAN modules
use Cwd;
use File::Basename;
use Locale::gettext;
use Try::Tiny;

# Path to binary
our $path;

# Let's take it so there will be no problems with module path
# determenation below
BEGIN
{
    $path = Cwd::abs_path(__FILE__);
    $path = File::Basename::dirname($path);
}

# Module path
use lib "$path/lib/";

# Our own modules
use cgi;
use html;
use i18n;

# Global Variables:
#
#  - CGI
our $cgi = undef;

################################################################################
# CGI
################################################################################

sub cgi_init
{
    return cgi::init();
}

################################################################################

sub cgi_deinit
{
    my $cgi = shift;

    cgi::deinit($cgi);
}

################################################################################
# Main
################################################################################

# CGI
try
{
    $cgi = cgi_init  ();
           cgi_deinit($cgi);
}
catch
{
    die "[0]: $_";
};

# i18n & l10n
try
{
    chdir $path;

    i18n::init($cgi);
}
catch
{
    die "[1]: $_";
};

# HTML
print ${cgi::header_get($cgi)};
print ${html::header_get(html::title_get(gettext('LoveCrypt')))};

print html::INDENT1;

print << ';';


    </div>
    </div>
    <div id="main">
    <div id="centr">
;

print '
    <div id="centr_full">
        <div id="tile">
            <p><a href="/lovehive/"><img src="/lovehive/img/lovehive-libre/avatar.png"></a></p>
            <p><h1><a href="/lovehive/">', gettext('LoveHive'), '</a></h1></p>
        </div>
        <div id="tile">
            <p><h1><a href="/lovewind/">', gettext('LoveWind'), '</a></h1></p>
        </div>
    </div>', "\n";

print ${html::footer_get()};
