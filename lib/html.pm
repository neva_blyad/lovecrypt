=pod

=encoding UTF-8

=head1 NAME

LoveCrypt
html.pm

=head1 VERSION

0.01

=head1 DESCRIPTION

In 2006, Russian company SUP bought the LiveJournal, ruining the original engine
 by adding a police tracking system and tons of advertising.

In 2014, they've closed the official LiveJournal code repository.

LoveCrypt is fork of LiveJournal before its acquisition by SUP.

We retrieved the source code from Wayback Machive targeted at 2003:

https://web.archive.org/web/20070214155614/http://www.livejournal.org:80/download/code/livejournal-2003082500.tar.gz

No ads, no police tracking and other shit.

=head1 COPYRIGHT AND LICENSE

Copyright (C) 1994-2003 LiveJournal.com, Inc., a subsidiary of Six Apart, Ltd.
Copyright (C) 2019-2021 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
                                        <neva_blyad@lovecri.es>

This file is part of LoveCrypt.

LoveCrypt is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LoveCrypt is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LoveCrypt.  If not, see <https://www.gnu.org/licenses/>.

To see a copy of the license and information with full list of Non-GPL
components distributed by us please visit:
www.lovecrypt7k5p7uh.onion/copying/

=head1 AUTHORS

=over

=item Brad Fitzpatrick <bradfitz@bradfitz.com>

=item LiveJournal.com, Inc.

=item НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
                      <neva_blyad@lovecri.es>

=item Invisible Light

=back

=cut

# Declare HTML package
package html;

# We are serious about Perl
use strict;
use warnings;

# CPAN modules
use Locale::gettext;

# Our own modules
use i18n;

# Constants:
#
#  - HTML
use constant INDENT_WIDTH => 4;

use constant INDENT1 => ' ' x (1 * INDENT_WIDTH);
use constant INDENT2 => ' ' x (2 * INDENT_WIDTH);
use constant INDENT3 => ' ' x (3 * INDENT_WIDTH);
use constant INDENT4 => ' ' x (4 * INDENT_WIDTH);
use constant INDENT5 => ' ' x (5 * INDENT_WIDTH);
use constant INDENT6 => ' ' x (6 * INDENT_WIDTH);
use constant INDENT7 => ' ' x (7 * INDENT_WIDTH);
use constant INDENT8 => ' ' x (8 * INDENT_WIDTH);

################################################################################
# HTML
################################################################################

sub header_get
{
    my $title = shift;

    return \<< ";";
<!--
    Copyright (C) 1994-2003 LiveJournal.com, Inc., a subsidiary of Six Apart, Ltd.
    Copyright (C) 2019-2021 НЕВСКИЙ БЛЯДИНА <neva_blyad†lovecry.pt>
                                            <neva_blyad†lovecri.es>

    This file is part of LoveCrypt.

    LoveCrypt is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    LoveCrypt is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with LoveCrypt.  If not, see <https://www.gnu.org/licenses/>.

    To see a copy of the license and information with full list of Non-GPL
    components distributed by us please visit:
    www.lovecrypt7k5p7uh.onion/copying/
-->

<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="stylesheet" type="text/css" href="/style.css">
    <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico">
    <title>$$title</title>
</head>

<body>

    <div id="table">

    <div id="header">
    <div id="menu">

;
}

################################################################################

sub footer_get
{
    return \('
    </div>
    </div>
    <div id="footer">
    <div id="menu">
    
    <span id="menu"><a href="/copying/">' . gettext('Copying') . "</a></span>

    </div>
    </div>

    </div>

</body>

</html>\n");
}

################################################################################

sub title_get
{
    my @title = @_;

    return \join ' † ', @title;
}

return 1;
