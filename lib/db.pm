=pod

=encoding UTF-8

=head1 NAME

LoveCrypt
db.pm

=head1 VERSION

0.01

=head1 DESCRIPTION

In 2006, Russian company SUP bought the LiveJournal, ruining the original engine
 by adding a police tracking system and tons of advertising.

In 2014, they've closed the official LiveJournal code repository.

LoveCrypt is fork of LiveJournal before its acquisition by SUP.

We retrieved the source code from Wayback Machive targeted at 2003:

https://web.archive.org/web/20070214155614/http://www.livejournal.org:80/download/code/livejournal-2003082500.tar.gz

No ads, no police tracking and other shit.

=head1 COPYRIGHT AND LICENSE

Copyright (C) 1994-2003 LiveJournal.com, Inc., a subsidiary of Six Apart, Ltd.
Copyright (C) 2019-2021 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
                                        <neva_blyad@lovecri.es>

This file is part of LoveCrypt.

LoveCrypt is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LoveCrypt is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LoveCrypt.  If not, see <https://www.gnu.org/licenses/>.

To see a copy of the license and information with full list of Non-GPL
components distributed by us please visit:
www.lovecrypt7k5p7uh.onion/copying/

=head1 AUTHORS

=over

=item Brad Fitzpatrick <bradfitz@bradfitz.com>

=item LiveJournal.com, Inc.

=item НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
                      <neva_blyad@lovecri.es>

=item Invisible Light

=back

=cut

# Declare database package
package db;

# We are serious about Perl
use strict;
use warnings;

# CPAN modules
use DBI;

################################################################################
# Database
################################################################################

sub connect
{
    my $srv;
    my $db;

    require "$ENV{'LJHOME'}/cgi-bin/ljconfig.pl";

    $srv = 'master';
    $db  = DBI->connect("DBI:mysql:$LJ::DBINFO{$srv}->{'dbname'}:$LJ::DBINFO{$srv}->{'host'}",
                        $LJ::DBINFO{$srv}->{'user'},
                        $LJ::DBINFO{$srv}->{'pass'},
                        {'PrintWarn'  => 0,
                         'PrintError' => 0,
                         'RaiseError' => 1});

    return $db;
}

################################################################################

sub disconnect
{
    my $db = shift;

    $db->disconnect();
}

return 1;
