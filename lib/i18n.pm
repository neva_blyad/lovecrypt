=pod

=encoding UTF-8

=head1 NAME

LoveCrypt
i18n.pm

=head1 VERSION

0.01

=head1 DESCRIPTION

In 2006, Russian company SUP bought the LiveJournal, ruining the original engine
 by adding a police tracking system and tons of advertising.

In 2014, they've closed the official LiveJournal code repository.

LoveCrypt is fork of LiveJournal before its acquisition by SUP.

We retrieved the source code from Wayback Machive targeted at 2003:

https://web.archive.org/web/20070214155614/http://www.livejournal.org:80/download/code/livejournal-2003082500.tar.gz

No ads, no police tracking and other shit.

=head1 COPYRIGHT AND LICENSE

Copyright (C) 1994-2003 LiveJournal.com, Inc., a subsidiary of Six Apart, Ltd.
Copyright (C) 2019-2021 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
                                        <neva_blyad@lovecri.es>

This file is part of LoveCrypt.

LoveCrypt is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LoveCrypt is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LoveCrypt.  If not, see <https://www.gnu.org/licenses/>.

To see a copy of the license and information with full list of Non-GPL
components distributed by us please visit:
www.lovecrypt7k5p7uh.onion/copying/

=head1 AUTHORS

=over

=item Brad Fitzpatrick <bradfitz@bradfitz.com>

=item LiveJournal.com, Inc.

=item НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
                      <neva_blyad@lovecri.es>

=item Invisible Light

=back

=cut

# Declare CGI package
package i18n;

# We are serious about Perl
use strict;
use warnings;

# CPAN modules
use Geo::IP;
use Locale::gettext;
use POSIX;
use Tie::IxHash;

# Constants:
#
#  - i18n & l10n
use constant DIR     => 'locale/';
use constant DB      => 'dl/GeoLiteCity.dat';
use constant DOMAIN  => 'messages';
use constant CHARSET => 'UTF-8';

# Global Variables:
#
#  - i18n & l10n
tie our %locale, 'Tie::IxHash'; %locale = ();

%locale = ('en' => ['C',           'English'],
           'ru' => ['ru_RU.UTF-8', 'Русский']); # Supported translations

################################################################################
# i18n & l10n
################################################################################

sub init
{
    my $locale;

    my $cgi = shift;

    # Set locale using the user IP / hostname
    $locale = set(\$cgi->remote_host());

    return $locale;
}

################################################################################

sub get
{
    # Get locale
    return setlocale(LC_MESSAGES);
}

###############################################################################

sub set
{
    my $geo;
    my $rec;
    my $code;
    my $locale;

    my $host = shift;

    # Determine the locale by IP / hostname
    $geo    = Geo::IP->open(DB, GEOIP_MEMORY_CACHE | GEOIP_CHECK_CACHE);
    $rec    = $geo->record_by_name($$host);
    $code   = defined $rec           ? $rec->country_code : 'ru';
    $locale = exists  $locale{$code} ? \$locale{$code}[0] : \$locale{'ru'}[0];

    # Setup BML translations
    require "$ENV{'LJHOME'}/cgi-bin/Apache/BML.pm";
    BML::set_language($code, \&LJ::Lang::get_text);

    # Setup locale
    setlocale(LC_MESSAGES, $$locale);

    $ENV{'LANG'}        = $$locale;
    $ENV{'LC_MESSAGES'} = $$locale;

    if ($$locale eq 'C')
    {
        textdomain(NULL);
    }
    else
    {
        textdomain(DOMAIN);
        bindtextdomain(DOMAIN, DIR);
        bind_textdomain_codeset(DOMAIN, CHARSET);
    }

    return $locale;
}

return 1;
