#!/usr/bin/env perl

=pod

=encoding UTF-8

=head1 NAME

LoveCrypt
index.pl

=head1 VERSION

0.01

=head1 DESCRIPTION

In 2006, Russian company SUP bought the LiveJournal, ruining the original engine
 by adding a police tracking system and tons of advertising.

In 2014, they've closed the official LiveJournal code repository.

LoveCrypt is fork of LiveJournal before its acquisition by SUP.

We retrieved the source code from Wayback Machive targeted at 2003:

https://web.archive.org/web/20070214155614/http://www.livejournal.org:80/download/code/livejournal-2003082500.tar.gz

No ads, no police tracking and other shit.

=head1 COPYRIGHT AND LICENSE

Copyright (C) 1994-2003 LiveJournal.com, Inc., a subsidiary of Six Apart, Ltd.
Copyright (C) 2019-2021 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
                                        <neva_blyad@lovecri.es>

This file is part of LoveCrypt.

LoveCrypt is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LoveCrypt is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LoveCrypt.  If not, see <https://www.gnu.org/licenses/>.

To see a copy of the license and information with full list of Non-GPL
components distributed by us please visit:
www.lovecrypt7k5p7uh.onion/copying/

=head1 AUTHORS

=over

=item Brad Fitzpatrick <bradfitz@bradfitz.com>

=item LiveJournal.com, Inc.

=item НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
                      <neva_blyad@lovecri.es>

=item Invisible Light

=back

=cut

# We are serious about Perl
use strict;
use warnings;

# Some I/O functions should fuck up everything
use autodie 'chdir';

# CPAN modules
use CGI;
use Cwd;
use DBI;
use File::Basename;
use HTML::Entities;
use Locale::gettext;
use POSIX;
use Regexp::Common;
use Tie::IxHash;
use Try::Tiny;

# Path to binary
our $path;

# Let's take it so there will be no problems with module path
# determenation below
BEGIN
{
    $path = Cwd::abs_path(__FILE__);
    $path = File::Basename::dirname($path);
}

# Module path
use lib "$path/../lib/";

# Our own modules
use cgi;
use db;
use html;
use i18n;

# Constants:
#
#  - Database,
use constant DB_SELECT_USERS_CNT => 0;
use constant DB_SELECT_USERS     => 1;

#  - HTML
use constant PAGE_LIMIT          => 188;

use constant PAGES_MAX           => 14;
use constant PAGES_LEFT          => int(PAGES_MAX / 2);
use constant PAGES_RIGHT         => int(PAGES_MAX / 2);

use constant COL_NUM             => 4;
use constant COL_IDX_LAST        => COL_NUM - 1;

# Global Variables:
#
#  - CGI,
our $cgi              = undef;

our %cgi_page         = ();

our %cgi_service      = ();
our %cgi_church_title = ();
our %cgi_commandery   = ();
our %cgi_sort_by      = ();
our %cgi_sort_order   = ();

our $cgi_pages        = undef;

#  - Database,
our $db               = undef;

our $db_sql           = undef;

our $db_users         = undef;
our $db_users_cnt     = undef;
our $db_services      = undef;
our $db_church_titles = undef;
our $db_commanderies  = undef;

#  - Data,
our @data             = ();

#  - HTML
our $url_pages        = undef;
our $url_pages_       = undef;
our $td_idx_last      = undef;
our $rowspan1         = undef;
our $rowspan2         = undef;
our $rowspan3         = undef;
our $pages_begin      = undef;
our $pages_end        = undef;

tie our %sort_by,    'Tie::IxHash'; %sort_by    = ();
tie our %sort_order, 'Tie::IxHash'; %sort_order = ();

our     @sort_by    = ();
our     @sort_order = ();

%sort_by    = ('user'         => '',
               'name'         => '',
               'service'      => '',
               'church_title' => '',
               'commandery'   => '');

%sort_order = ('ASC'          => '',
               'DESC'         => '');

@sort_by    = keys %sort_by;
@sort_order = keys %sort_order;

################################################################################
# CGI
################################################################################

sub cgi_init
{
    my $cgi;

    $cgi = cgi::init();

    $cgi_page        {'val'         } = $cgi->url_param('page');
    $cgi_page        {'verified_val'} = defined $cgi_page{'val'} && $cgi_page{'val'} =~ /^$RE{'num'}{'int'}\z/ && $cgi_page{'val'} > 1 ?
                                            $cgi_page{'val'} : undef;

    $cgi_service     {'val'         } = [$cgi->url_param('service')];
    $cgi_service     {'verified_val'} = defined $cgi_service{'val'}->[0] ?
                                            $cgi_service{'val'} : [];

    $cgi_church_title{'val'         } = [$cgi->url_param('church_title')];
    $cgi_church_title{'verified_val'} = defined $cgi_church_title{'val'}->[0] ?
                                            $cgi_church_title{'val'} : [];

    $cgi_commandery  {'val'         } = [$cgi->url_param('commandery')];
    $cgi_commandery  {'verified_val'} = defined $cgi_commandery{'val'}->[0] ?
                                            $cgi_commandery{'val'} : [];

    $cgi_sort_by     {'val'         } = $cgi->url_param('sort_by');
    $cgi_sort_by     {'verified_val'} = defined $cgi_sort_by{'val'} && grep({ $cgi_sort_by{'val'} eq $_ } @sort_by) ?
                                            $cgi_sort_by{'val'} : undef;

    $cgi_sort_order  {'val'         } = $cgi->url_param('sort_order');
    $cgi_sort_order  {'verified_val'} = defined $cgi_sort_order{'val'} && grep({ $cgi_sort_order{'val'} eq $_ } @sort_order) ?
                                            $cgi_sort_order{'val'} : undef;

    return $cgi;
}

################################################################################

sub cgi_page_set
{
    my $cgi = shift;

    if (defined $db_users_cnt)
    {
            $cgi_pages = ceil($db_users_cnt / PAGE_LIMIT);
        if ($cgi_pages == 0)
        {
            $cgi_page{'final_val'} = 0;
        }
        else
        {
            $cgi_page{'final_val'} = defined $cgi_page{'verified_val'} &&
                                             $cgi_page{'verified_val'} <= $cgi_pages ?
                $cgi_page{'verified_val'} :
                1;
        }
    }
}

################################################################################

sub cgi_service_set
{
    my $cgi = shift;

    $cgi_service{'final_val'} = [];

    foreach my $service_idx (0 .. $#{$cgi_service{'verified_val'}})
    {
        next if $cgi_service{'verified_val'}->[$service_idx] eq '' ||
                $cgi_service{'verified_val'}->[$service_idx] =~ /\n|\r/;

        push @{$cgi_service{'final_val'}}, $cgi_service{'verified_val'}->[$service_idx];
    }
}

################################################################################

sub cgi_church_title_set
{
    my $cgi = shift;

    $cgi_church_title{'final_val'} = [];

    foreach my $church_title_idx (0 .. $#{$cgi_church_title{'verified_val'}})
    {
        next if $cgi_church_title{'verified_val'}->[$church_title_idx] eq '' ||
                $cgi_church_title{'verified_val'}->[$church_title_idx] =~ /\n|\r/;

        push @{$cgi_church_title{'final_val'}}, $cgi_church_title{'verified_val'}->[$church_title_idx];
    }
}

################################################################################

sub cgi_commandery_set
{
    my $cgi = shift;

    $cgi_commandery{'final_val'} = [];

    foreach my $commandery_idx (0 .. $#{$cgi_commandery{'verified_val'}})
    {
        next if $cgi_commandery{'verified_val'}->[$commandery_idx] eq '' ||
                $cgi_commandery{'verified_val'}->[$commandery_idx] =~ /\n|\r/;

        push @{$cgi_commandery{'final_val'}}, $cgi_commandery{'verified_val'}->[$commandery_idx];
    }
}

################################################################################

sub cgi_deinit
{
    my $cgi = shift;

    cgi::deinit($cgi);
}

################################################################################
# Database
################################################################################

sub db_connect
{
    return db::connect();
}

################################################################################

sub db_lock
{
    my $db = shift;

    $db->do(<< ';');
LOCK TABLES user         AS user1         READ,
            user         AS user2         READ,
            user         AS user3         READ,
            userprop                      READ,
            userprop     AS userprop1     READ,
            userprop     AS userprop2     READ,
            userprop     AS userprop3     READ,
            userproplist                  READ,
            userproplist AS userproplist1 READ,
            userproplist AS userproplist2 READ,
            userproplist AS userproplist3 READ,
            service                       READ,
            service      AS service1      READ,
            service      AS service2      READ,
            churchtitles                  READ,
            churchtitles AS churchtitles1 READ,
            churchtitles AS churchtitles2 READ,
            commandery                    READ,
            commandery   AS commandery1   READ,
            commandery   AS commandery2   READ,
            userpic                       READ
;
}

################################################################################

sub db_select
{
    my $query;

    my $db     = shift;
    my $action = shift;

    if ($action == DB_SELECT_USERS_CNT)
    {
        # Form the start part of the SQL query
        $db_sql = << ';';
SELECT COUNT(*)
FROM

(
    SELECT    user1.userid AS user_id, user1.defaultpicid AS pic_id, UNIX_TIMESTAMP(userpic.picdate) AS ts, user1.user AS user,
                  user1.name AS name, service.name AS service
    FROM      user AS user1

    JOIN      userprop     AS userprop1
    USING     (userid)
    JOIN      userproplist AS userproplist1
    USING     (upropid)
    JOIN      service
    LEFT JOIN userpic
    ON        user1.userid       = userpic.userid
    AND       user1.defaultpicid = userpic.picid

    WHERE     userprop1.value    = service.serviceid
    AND       userproplist1.name = 'serviceid'
) AS tbl1

JOIN

(
    SELECT    user2.userid AS user_id, churchtitles.name AS church_title
    FROM      user AS user2

    JOIN      userprop     AS userprop2
    USING     (userid)
    JOIN      userproplist AS userproplist2
    USING     (upropid)
    JOIN      churchtitles

    WHERE     userprop2.value    = churchtitles.churchtitleid
    AND       userproplist2.name = 'churchtitleid'
) AS tbl2

USING (user_id)

JOIN

(
    SELECT    user3.userid AS user_id, commandery.name AS commandery
    FROM      user AS user3

    JOIN      userprop     AS userprop3
    USING     (userid)
    JOIN      userproplist AS userproplist3
    USING     (upropid)
    JOIN      commandery

    WHERE     userprop3.value    = commandery.commanderyid
    AND       userproplist3.name = 'commanderyid'
) AS tbl3

USING (user_id)
;

        # Add service to the WHERE clause
        if (@{$cgi_service{'final_val'}} > 0)
        {
            if (@{$cgi_service{'final_val'}} > 1 && (@{$cgi_church_title{'final_val'}} > 0 ||
                                                     @{$cgi_commandery  {'final_val'}} > 0))
            {
                $db_sql .= '
WHERE (service = ?';
                $db_sql .= ' OR
       service = ?' for 1 .. $#{$cgi_service{'final_val'}};
                $db_sql .= ')';
            }
            else
            {
                $db_sql .= '
WHERE service = ?';
                $db_sql .= ' OR
      service = ?'  for 1 .. $#{$cgi_service{'final_val'}};
            }

            $db_sql .= << ';';

;
        }

        # Add church title to the WHERE clause
        if (@{$cgi_church_title{'final_val'}} > 0)
        {
            if (@{$cgi_church_title{'final_val'}} > 1 && (@{$cgi_service   {'final_val'}} > 0 ||
                                                          @{$cgi_commandery{'final_val'}} > 0))
            {
                $db_sql .= @{$cgi_service{'final_val'}} > 0 ? '
AND   (church_title = ?' : '
WHERE (church_title = ?';
                $db_sql .= ' OR
       church_title = ?' for 1 .. $#{$cgi_church_title{'final_val'}};
                $db_sql .= ')';
            }
            else
            {
                $db_sql .= @{$cgi_service{'final_val'}} > 0 ? '
AND   church_title = ?' : '
WHERE church_title = ?';
                $db_sql .= ' OR
      church_title = ?'  for 1 .. $#{$cgi_church_title{'final_val'}};
            }

            $db_sql .= << ';';

;
        }

        # Add commandery to the WHERE clause
        if (@{$cgi_commandery{'final_val'}} > 0)
        {
            if (@{$cgi_commandery{'final_val'}} > 1 && (@{$cgi_service     {'final_val'}} > 0 ||
                                                        @{$cgi_church_title{'final_val'}} > 0))
            {
                $db_sql .= '
AND   (commandery = ?';
                $db_sql .= ' OR
       commandery = ?' for 1 .. $#{$cgi_commandery{'final_val'}};
                $db_sql .= ')';
            }
            else
            {
                $db_sql .= @{$cgi_service     {'final_val'}} > 0 ||
                              @{$cgi_church_title{'final_val'}} > 0 ? '
AND   commandery = ?' : '
WHERE commandery = ?';
                $db_sql .= ' OR
      commandery = ?'  for 1 .. $#{$cgi_commandery{'final_val'}};
            }

            $db_sql .= << ';';

;
        }

        $query = $db->prepare($db_sql);

        foreach my $service_idx      (0 .. $#{$cgi_service     {'final_val'}})
        { $query->bind_param($service_idx + 1,                                                                         $cgi_service     {'final_val'}->[$service_idx],      DBI::SQL_VARCHAR); }
        foreach my $church_title_idx (0 .. $#{$cgi_church_title{'final_val'}})
        { $query->bind_param($#{$cgi_service{'final_val'}} + 2 + $church_title_idx,                                    $cgi_church_title{'final_val'}->[$church_title_idx], DBI::SQL_VARCHAR); }
        foreach my $commandery_idx   (0 .. $#{$cgi_commandery  {'final_val'}})
        { $query->bind_param($#{$cgi_service{'final_val'}} + $#{$cgi_church_title{'final_val'}} + 3 + $commandery_idx, $cgi_commandery  {'final_val'}->[$commandery_idx],   DBI::SQL_VARCHAR); }

                         $query->execute();
        $db_users_cnt = ($query->fetchrow_array())[0];

    }
    #elsif ($action == DB_SELECT_USERS)
    else
    {
        # Form the start part of the SQL query
        $db_sql =~ s/SELECT COUNT\(\*\)/SELECT user_id, pic_id, ts, user, name, service, church_title, commandery/;

        # Add the LIMIT clause
        $db_sql .= '

LIMIT ' . PAGE_LIMIT . ($cgi_page{'final_val'} > 1 ? ' OFFSET ' . ($cgi_page{'final_val'} - 1) * PAGE_LIMIT :
                                                     '') . "\n";

        $query = $db->prepare($db_sql);

        foreach my $service_idx      (0 .. $#{$cgi_service     {'final_val'}})
        { $query->bind_param($service_idx + 1,                                                                         $cgi_service     {'final_val'}->[$service_idx],      DBI::SQL_VARCHAR); }
        foreach my $church_title_idx (0 .. $#{$cgi_church_title{'final_val'}})
        { $query->bind_param($#{$cgi_service{'final_val'}} + 2 + $church_title_idx,                                    $cgi_church_title{'final_val'}->[$church_title_idx], DBI::SQL_VARCHAR); }
        foreach my $commandery_idx   (0 .. $#{$cgi_commandery  {'final_val'}})
        { $query->bind_param($#{$cgi_service{'final_val'}} + $#{$cgi_church_title{'final_val'}} + 3 + $commandery_idx, $cgi_commandery  {'final_val'}->[$commandery_idx],   DBI::SQL_VARCHAR); }

                    $query->execute();
        $db_users = $query->fetchall_arrayref();

        $db_services = $db->selectall_arrayref(<< ';');
SELECT service, CASE WHEN users IS NOT NULL
                THEN users
                ELSE 0
                END AS users
FROM

(
    SELECT          service1.serviceid AS service_id, COUNT(*) AS users
    FROM            service AS service1
    JOIN            userprop
    JOIN            userproplist
    USING           (upropid)

    WHERE           userprop.value    = service1.serviceid
    AND             userproplist.name = 'serviceid'

    GROUP BY        service_id
) AS tbl1

RIGHT JOIN

(
    SELECT DISTINCT service2.serviceid AS service_id, service2.name AS service
    FROM            service AS service2
) AS tbl2

USING (service_id)

ORDER BY service_id
;

        $db_church_titles = $db->selectall_arrayref(<< ';');
SELECT church_title, CASE WHEN users IS NOT NULL
                     THEN 1
                     ELSE 0
                     END AS users
FROM

(
    SELECT          churchtitles1.churchtitleid AS church_title_id, COUNT(*) AS users
    FROM            churchtitles AS churchtitles1
    JOIN            userprop
    JOIN            userproplist
    USING           (upropid)

    WHERE           userprop.value    = churchtitles1.churchtitleid
    AND             userproplist.name = 'churchtitleid'

    GROUP BY        church_title_id
) AS tbl1

RIGHT JOIN

(
    SELECT DISTINCT churchtitles2.churchtitleid AS church_title_id, churchtitles2.name AS church_title
    FROM            churchtitles AS churchtitles2
) AS tbl2

USING (church_title_id)

ORDER BY church_title_id
;

        $db_commanderies = $db->selectall_arrayref(<< ';');
SELECT commandery, CASE WHEN users IS NOT NULL
                   THEN users
                   ELSE 0
                   END AS users
FROM

(
    SELECT          commandery1.commanderyid AS commandery_id, COUNT(*) AS users
    FROM            commandery AS commandery1
    JOIN            userprop
    JOIN            userproplist
    USING           (upropid)

    WHERE           userprop.value    = commandery1.commanderyid
    AND             userproplist.name = 'commanderyid'

    GROUP BY        commandery_id
) AS tbl1

RIGHT JOIN

(
    SELECT DISTINCT commandery2.commanderyid AS commandery_id, commandery2.name AS commandery
    FROM            commandery AS commandery2
) AS tbl2

USING (commandery_id)

ORDER BY commandery_id
;
    }
}

################################################################################

sub db_unlock
{
    my $db = shift;

    $db->do(<< ';');
UNLOCK TABLES
;
}

################################################################################

sub db_disconnect
{
    my $db = shift;

    db::disconnect($db);
}

################################################################################
# Data
################################################################################

sub data_translate
{
    # Read database handler.
    # Translate service, church title, commandery via BML.
    foreach my $user (@$db_users)
    {
        my ($user_id, $pic_id, $ts, $user,
                $name, $service, $church_title, $commandery) = @$user;

        # Translate service via BML
        $service =  lc $service;
        $service =~ s/ /_/g;
        $service =  $BML::ML{"label.service.$service"};

        # Translate church title via BML
        $church_title =  lc $church_title;
        $church_title =~ s/ /_/g;
        $church_title =  $BML::ML{"label.churchtitle.$church_title"};

        # Translate commandery via BML
        $commandery = lc $commandery;
        $commandery = $BML::ML{"label.commandery.$commandery"};

        # Form new array with translated strings
        push @data, [$user_id, $pic_id, $ts, $user,
                         $name, $service, $church_title, $commandery];
    }
}

################################################################################

sub data_sort
{
    # Sort the result from database
    if ($cgi_sort_order{'verified_val'} eq 'DESC')
    {
        @data = sort
        {
            my (undef, undef, undef, $a_user,
                    $a_name, $a_service, $a_church_title, $a_commandery) = @$a;
            my (undef, undef, undef, $b_user,
                    $b_name, $b_service, $b_church_title, $b_commandery) = @$b;

            return ($cgi_sort_by{'verified_val'} eq 'user'         && $b_user         cmp $a_user        ) ||
                   ($cgi_sort_by{'verified_val'} eq 'name'         && $b_name         cmp $a_name        ) ||
                   ($cgi_sort_by{'verified_val'} eq 'service'      && $b_service      cmp $a_service     ) ||
                   ($cgi_sort_by{'verified_val'} eq 'church_title' && $b_church_title cmp $a_church_title) ||
                   ($cgi_sort_by{'verified_val'} eq 'commandery'   && $b_commandery   cmp $a_commandery  ) ||

                                                                      $a_user         cmp $b_user          ||
                                                                      $a_name         cmp $b_name          ||
                                                                      $a_service      cmp $b_service       ||
                                                                      $a_church_title cmp $b_church_title  ||
                                                                      $a_commandery   cmp $b_commandery;
        } @data;
    }
    else
    {
        @data = sort
        {
            my (undef, undef, undef, $a_user,
                    $a_name, $a_service, $a_church_title, $a_commandery) = @$a;
            my (undef, undef, undef, $b_user,
                    $b_name, $b_service, $b_church_title, $b_commandery) = @$b;

            return ($cgi_sort_by{'verified_val'} eq 'user'         && $a_user         cmp $b_user        ) ||
                   ($cgi_sort_by{'verified_val'} eq 'name'         && $a_name         cmp $b_name        ) ||
                   ($cgi_sort_by{'verified_val'} eq 'service'      && $a_service      cmp $b_service     ) ||
                   ($cgi_sort_by{'verified_val'} eq 'church_title' && $a_church_title cmp $b_church_title) ||
                   ($cgi_sort_by{'verified_val'} eq 'commandery'   && $a_commandery   cmp $b_commandery  ) ||

                                                                      $a_user         cmp $b_user          ||
                                                                      $a_name         cmp $b_name          ||
                                                                      $a_service      cmp $b_service       ||
                                                                      $a_church_title cmp $b_church_title  ||
                                                                      $a_commandery   cmp $b_commandery;
        } @data;
    }
}

################################################################################
# Main
################################################################################

# CGI
try
{
    $cgi = cgi_init();
           cgi_service_set     ($cgi);
           cgi_church_title_set($cgi);
           cgi_commandery_set  ($cgi);
}
catch
{
    die "[0]: $_";
};

# i18n & l10n
try
{
    chdir "$path/../";

    i18n::init($cgi);
}
catch
{
    die "[1]: $_";
};

$sort_by{'user'        } = gettext('Churched Name'     );
$sort_by{'name'        } = gettext('Full Churched Name');
$sort_by{'service'     } = gettext('Service'           );
$sort_by{'church_title'} = gettext('Church Title'      );
$sort_by{'commandery'  } = gettext('Commandery'        );

$sort_order{'ASC' } = gettext('Asc.' );
$sort_order{'DESC'} = gettext('Desc.');

# Database.
#
# View churched.
try
{
    $db = db_connect();
          db_lock($db);
          db_select($db, DB_SELECT_USERS_CNT);
}
catch
{
    die "[2]: $_";
};

# CGI
try
{
    cgi_page_set($cgi);
}
catch
{
    die "[3]: $_";
};

# Database.
#
# View churched.
try
{
    db_select($db, DB_SELECT_USERS);
    db_unlock($db);
    db_disconnect($db);
}
catch
{
    die "[4]: $_";
};

# Data
try
{
    data_translate();
    data_sort();
}
catch
{
    die "[5]: $_";
};

# HTML
$CGI::USE_PARAM_SEMICOLONS = 0;

$cgi->delete('page');

    $url_pages = $cgi->query_string();
if ($url_pages eq '')
{
    $url_pages_ = '';
}
else
{
    $url_pages  = "?$url_pages";
    $url_pages_ = encode_entities($url_pages, '<>&"');
}

# Show the churched page
print ${cgi::header_get($cgi)};
print ${html::header_get(html::title_get(gettext('LoveWind'),
                                         gettext('LoveCrypt')))};

# Print filter form. It can be used for the churched sorting.
$rowspan1 = $#$db_services      == -1 ? ' rowspan="' . ($#$db_church_titles == -1 && $#$db_commanderies == -1 ? 2 : $#$db_church_titles > $#$db_commanderies ? $#$db_church_titles + 2 : $#$db_commanderies + 2) . '"' : '';
$rowspan2 = $#$db_church_titles == -1 ? ' rowspan="' . ($#$db_commanderies == -1 ? 2 : $#$db_commanderies + 2) . '"' : '';
$rowspan3 = $#$db_commanderies  == -1 ? ' rowspan="2"' : '';

print html::INDENT1, qq(<form id="filters">
        <table id="filters">
            <tr><th$rowspan1 id="service">), gettext('Service'), qq(</th><th$rowspan2 id="church_title">), gettext('Church Title'), qq(</th><th$rowspan3 id="commandery">), gettext('Commandery'), q(</th><th colspan="2" id="sort">), gettext('Sort By'), qq(</th>\n);

if    ($#$db_services      == -1 &&
       $#$db_church_titles == -1 &&
       $#$db_commanderies  == -1)
{
    $td_idx_last = 0;
}
elsif ($#$db_services > $#$db_church_titles &&
       $#$db_services > $#$db_commanderies)
{
    $td_idx_last = $#$db_services;
}
elsif ($#$db_church_titles > $#$db_services &&
       $#$db_church_titles > $#$db_commanderies)
{
    $td_idx_last = $#$db_church_titles;
}
else
{
    $td_idx_last = $#$db_commanderies;
}

$rowspan1 = '';

foreach my $td (0 .. $td_idx_last)
{
    my ($val, $val_out, $val_cnt);
    my  $id;
    my  $avail;

    # Output next table cell:
    print << ";";
            <tr>
;

    #  - service filter,
    if ($td <= $#$db_services)
    {
        ($val, $val_cnt) = @{$db_services->[$td]};

        if ($val_cnt > 0)
        {
            $id    = '';
            $avail = grep({ $val eq $_ } @{$cgi_service{'final_val'}}) ? ' checked'  : '';
        }
        else
        {
            $id    = ' id="grey"';
            $avail = ' disabled';
        }

        $val_out =  lc $val;
        $val_out =~ s/ /_/g;
        $val_out =  $BML::ML{"label.service.$val_out"};
        $val_out =  encode_entities($val_out, '<>&');

        $val = encode_entities($val, '<>&"');

        $rowspan1 = ' rowspan="' . ($#$db_church_titles > $#$db_commanderies ? $#$db_church_titles - $td + 1 :
                                                                               $#$db_commanderies  - $td + 1) . '"' if $td == $#$db_services && ($td < $#$db_church_titles ||
                                                                                                                                                 $td < $#$db_commanderies);

        print << ";";
                <td$rowspan1 id="service"><label$id><input type="checkbox" name="service" value="$val"$avail>$val_out ($val_cnt)</label></td>
;
    }

    # gettext() hack comment """

    #  - church title filter,
    if ($td <= $#$db_church_titles)
    {
        ($val, $val_cnt) = @{$db_church_titles->[$td]};

        if ($val_cnt > 0)
        {
            $id    = '';
            $avail = grep({ $val eq $_ } @{$cgi_church_title{'final_val'}}) ? ' checked'  : '';
        }
        else
        {
            $id    = ' id="grey"';
            $avail = ' disabled';
        }

        $val_out =  lc $val;
        $val_out =~ s/ /_/g;
        $val_out =  $BML::ML{"label.churchtitle.$val_out"};
        $val_out =  encode_entities($val_out, '<>&');

        $val = encode_entities($val, '<>&"');

        $rowspan2 = ' rowspan="' . ($#$db_commanderies - $td + 1) . '"' if $td == $#$db_church_titles && $td < $#$db_commanderies;

        print << ";";
                <td$rowspan2 id="church_title"><label$id><input type="checkbox" name="church_title" value="$val"$avail>$val_out ($val_cnt)</label></td>
;
    }

    #  - commandery filter,
    if ($td <= $#$db_commanderies)
    {
        ($val, $val_cnt) = @{$db_commanderies->[$td]};

        if ($val_cnt > 0)
        {
            $id    = '';
            $avail = grep({ $val eq $_ } @{$cgi_commandery{'final_val'}}) ? ' checked'  : '';
        }
        else
        {
            $id    = ' id="grey"';
            $avail = ' disabled';
        }

        $val_out = lc $val;
        $val_out = $BML::ML{"label.commandery.$val_out"};
        $val_out = encode_entities($val_out, '<>&');

        $val = encode_entities($val, '<>&"');

        print << ";";
                <td id="commandery"><label$id><input type="checkbox" name="commandery" value="$val"$avail>$val_out ($val_cnt)</label></td>
;
    }

    #  - sort by, sort order,
    if ($td == 0)
    {
        print << ';';
                <td id="sort_by">
                    <select name="sort_by" autofocus>
;

        foreach my $sort_by (@sort_by)
        {
            my $sel;

            $sel = $sort_by eq $cgi_sort_by{'verified_val'} ? ' selected' : '';

            print << ";";
                        <option value="$sort_by"$sel>$sort_by{$sort_by}
;
        }

        print << ';';
                    </select>
                </td>
                <td id="sort_order">
                    <select name="sort_order">
;

        foreach my $sort_order (@sort_order)
        {
            my $sel;

            $sel = $sort_order eq $cgi_sort_order{'verified_val'} ? ' selected' : '';

            print << ";";
                        <option value="$sort_order"$sel>$sort_order{$sort_order}
;
        }

        print << ';';
                    </select>
                </td>
;
    }
}

print html::INDENT2, q(</table>
        <input type="submit" value="), gettext('Filter'), qq(">
    </form>\n\n);

print html::INDENT1;

print q(<span id="menu"><a href="/">), gettext('LoveCrypt'), q(</a></span>);

print << ';';


    </div>
    </div>
;

# Page menu
if ($cgi_page{'final_val'} > 0)
{
    my $tmp;

    $pages_begin = html::INDENT1 . q(<div id="header">
    <div id="pages">

    ) . gettext('You are viewing pages:');

    # First to current pages
    $tmp = $cgi_page{'final_val'} - PAGES_LEFT;

    if ($tmp < 1)
    {
        $tmp = 1;
    }
    elsif ($tmp > 1)
    {
        $pages_begin .= '<span id="page">...</span>';
    }

    foreach my $page ($tmp .. $cgi_page{'final_val'} - 1)
    {
        $pages_begin .= qq(<span id="page"><a href="/lovewind/$url_pages_) .
            ($url_pages_ eq '' ? '?' : '&amp;') . qq(page=$page">$page</a></span>);
    }

    # Current page
    $pages_begin .= qq(<span id="page">$cgi_page{'final_val'}</span>);

    # Current to last pages
    $tmp = $cgi_page{'final_val'} + PAGES_RIGHT;

    if ($tmp > $cgi_pages)
    {
        $tmp = $cgi_pages;
    }

    foreach my $page ($cgi_page{'final_val'} + 1 .. $tmp)
    {
        $pages_begin .= qq(<span id="page"><a href="/lovewind/$url_pages_) .
            ($url_pages_ eq '' ? '?' : '&amp;') . qq(page=$page">$page</a></span>);
    }

    if ($tmp < $cgi_pages)
    {
        $pages_begin .= '<span id="page">...</span>';
    }
    else
    {
        $pages_begin .= '.';
    }

    $tmp = ($cgi_page{'final_val'} - 1) * PAGE_LIMIT;

    $pages_begin .= '

    </div>
    </div>
    <div id="header">
    <div id="cnt">

    ' . gettext('Showing ') . ($tmp + 1)  . '–' . ($tmp + scalar @data) . gettext(' churched of ') . "$db_users_cnt.";

    $pages_end = << ';';


    </div>
    </div>
;
}
else
{
    $pages_begin = '';
    $pages_end   = '';
}

print $pages_begin, $pages_end;

print << ';';
    <div id="main">
    <div id="centr">

;

# Check we have any churched
if (@data == 0)
{
    # No churched case
    print html::INDENT1, '<div id="centr_full"><h1>', gettext('No LoveWind'), '</h1></div>';
}
else
{
    my $user_idx;
    my $row_idx_last;

    # Print main table with all churched
    print << ';';
    <table id="users">
;

    $user_idx     = 0;
    $row_idx_last = int($#data / COL_NUM);

    foreach my $row (0 .. $row_idx_last)
    {
        my  $col_idx_last;

        my (@user_id, @pic_id);
        my  @ts;
        my (@user, @name, @service, @church_title,
                @commandery);

        my @user_href;
        my @avatar;

        # Get data from database — next COL_LUM numbers
        $col_idx_last = $row == $row_idx_last ? $#data % COL_NUM :
                                                COL_IDX_LAST;

        foreach my $col (0 .. $col_idx_last)
        {
            # Get fields from data array
            ($user_id[$col], $pic_id[$col]) = map encode_entities($_, '<>&"'), @{$data[$user_idx + $col]}[0..1];
             $ts[$col] = @{$data[$user_idx + $col]}[2];
            ($user[$col], $name[$col], $service[$col], $church_title[$col],
                 $commandery[$col]) = map encode_entities($_, '<>&'), @{$data[$user_idx + $col]}[3..7];

            # Get links to churched
            $user_href[$col] = encode_entities($user[$col], '"');

            # Get the avatar URL
            $avatar[$col] = ($pic_id[$col] && $ts[$col]) ? "/lovehive/userpic/$pic_id[$col]/$user_id[$col]?$ts[$col]" :
                                                           '/lovewind/img/no_avatar.png';
        }

        # Print information — next COL_LUM numbers:
        #  - avatar,
                                                              print html::INDENT2, qq(<tr>\n);
        foreach my $col (0 .. $col_idx_last)                { print html::INDENT3, qq(<th id="avatar"><a href="/lovehive/allpics.bml?user=$user_href[$col]"><img src="$avatar[$col]" id="avatar"></a></th>\n); }
        foreach my $col ($col_idx_last + 1 .. COL_IDX_LAST) { print html::INDENT3, qq(<td rowspan="6" id="avatar"></td>\n); }

        #  - churched name,
                                                              print html::INDENT2, qq(<tr>\n);
        foreach my $col (0 .. $col_idx_last)                { print html::INDENT3, qq(<td id="main"><span id="profile"><a href="/lovehive/userinfo.bml?user=$user_href[$col]"><img src="/lovehive/img/userinfo.gif" id="icon"></a><a href="/lovehive/users/$user_href[$col]">$user[$col]</a></span></td>\n); }

        #  - full churched name,
                                                              print html::INDENT2, qq(<tr>\n);
        foreach my $col (0 .. $col_idx_last)                { print html::INDENT3, qq(<td id="main">$name[$col]</td>\n); }

        #  - service,
                                                              print html::INDENT2, qq(<tr>\n);
        foreach my $col (0 .. $col_idx_last)                { print html::INDENT3, qq(<td id="main_centr">$service[$col]</td>\n); }

        #  - church title,
                                                              print html::INDENT2, qq(<tr>\n);
        foreach my $col (0 .. $col_idx_last)                { print html::INDENT3, qq(<td id="main_centr">$church_title[$col]</td>\n); }

        #  - commandery
                                                              print html::INDENT2, qq(<tr>\n);
        foreach my $col (0 .. $col_idx_last)                { print html::INDENT3, qq(<td id="main_centr">$commandery[$col]</td>\n); }

        $user_idx += COL_NUM;
    }

    # Print end of the churched table
    print << ';';
    </table>
;
}

print $pages_end, $pages_begin, "\n";

print ${html::footer_get()};

# CGI
try
{
    cgi_deinit($cgi);
}
catch
{
    die "[6]: $_";
};
