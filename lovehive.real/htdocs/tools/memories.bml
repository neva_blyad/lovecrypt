<?_code

 use strict;
 use vars qw(%GET $head $title $body);

 my $dbr = LJ::get_db_reader();
 my $remote = LJ::get_remote();

 my $sth;

 $title = "";
 $head = "";
 $body = "";

 my $user = LJ::canonical_username($GET{'user'});
 if ($GET{'user'} && ! $user) {
     $body = $ML{'error.malformeduser'};
     return;
 }

 if ($user eq "" && defined $remote) {
     $user = $remote->{'user'};
 }

 my $u = LJ::load_user($user);
 unless ($u) {
     $title = $ML{'Error'};
     $body = $ML{'error.username_notfound'};
     return;
 }
 my $userid = $u->{'userid'};

 if ($u->{'journaltype'} eq "R") {
    LJ::load_user_props($u, "renamedto");
    return BML::redirect("/tools/memories.bml?user=$u->{'renamedto'}");
 }

 LJ::load_user_props($u, "opt_blockrobots") if $u->{'statusvis'} eq 'V';
 unless ($u->{'statusvis'} eq 'V' && ! $u->{'opt_blockrobots'}) {
     $head = LJ::robot_meta_tags();
 }

 if ($u->{'statusvis'} eq "S") {
     $title = $ML{'error.suspended.title'};
     $body = "<?h1 $ML{'error.suspended.name'} h1?><?p " . BML::ml('error.suspended.text',{'sitename'=>$LJ::SITENAME,'user'=>$user}) . " p?>";
     return;
 }

 if ($u->{'statusvis'} eq "D") {
     $title = $ML{'error.deleted.title'};
     $body = "<?h1 $ML{'error.deleted.name'} h1?><?p " . BML::ml('error.deleted.text',{'user'=>$user}) . " p?>";
     return;
 }

 if ($u->{'statusvis'} eq "X") {
     $title = $ML{'error.purged.title'};
     $body = "<?h1 $ML{'error.purged.name'} h1?><?p $ML{'error.purged.text'} p?>";
     return;
 }

 my $authasu = LJ::get_authas_user($GET{'authas'} || $user);
 my $authasarg;
 if ($authasu) {
     $body .= "<form class='postheading' method='get' action='memories.bml'>\n";
     $body .= LJ::make_authas_select($remote, { 'authas' => $authasu->{user} }) . "\n";
     $body .= LJ::html_hidden(keyword => $GET{keyword}) if $GET{keyword};
     $body .= "</form>\n\n";

     $user = $authasu->{user};
     $userid = $authasu->{userid};
     $authasarg = "authas=$authasu->{user}&";
 } else {
     $authasu = $remote;
 }

 my %filters = ("all" => $ML{'.filter.all'},
                "own" => BML::ml(".filter.own", { 'user' => $user }),
                "other" => $ML{'.filter.other'});
 my $filter = $GET{'filter'} || "all";
 unless (defined $filters{$filter}) { $filter = "all"; }

 my %sorts = ('memid' => $ML{'.sort.orderadded'},
              'des' => $ML{'.sort.description'},
              'user' => $ML{'.sort.journal'});
 my $sort = ($GET{'sortby'} || 'memid');
 unless (defined $sorts{$sort}) { $sort = 'memid'; }

 my $secwhere = "AND m.security='public'";
 if ($authasu) {
     if ($authasu->{userid} == $userid) {
         $secwhere = "";
     } elsif ($authasu->{'journaltype'} eq 'P' and LJ::is_friend($userid, $authasu->{'userid'})) {
         $secwhere = "AND m.security IN ('public', 'friends')";
     }
 }

 if ($GET{'keyword'}) 
 {
     my $qkw = $dbr->quote($GET{'keyword'});
     if ($GET{'keyword'} eq "*") {
         $title = $ML{'.title.memorable'};
         $body .= "<?h1 $ML{'.title.memorable'} h1?><?p " . BML::ml(".body.memorable", { 'user' => $user }) . " p?>";
     } else {
         $title = BML::ml(".title.keyword", { 'keyword' => $GET{'keyword'}, 'user' => $user });
         $body .= BML::ml(".body.keyword", { 'keyword' => $GET{'keyword'}, 'user' => $user });
     }

     $body .= "<form method='get' action='memories.bml'>";
     $body .= LJ::html_hidden(keyword => $GET{keyword}) if $GET{keyword};
     $body .= LJ::html_hidden(user => $GET{user}) if $GET{user};
     $body .= LJ::html_hidden(authas => $GET{user}) if $GET{authas};
     $body .= "$ML{'.form.sort'} <select name='sortby'>";
     foreach my $sorttype (qw(memid des user)) {
         my $sel = $sort eq $sorttype ? 'selected' : '';
         $body .= "<option id='$sorttype' value='$sorttype' $sel><label for='$sorttype'>$sorts{$sorttype}</label>\n";
     }
     $body .= "</select> <input type='submit' value='$ML{'.form.switch'}'>";
     $body .= "</form>\n";
     $body .= "<a href=\"/tools/memories.bml?${authasarg}user=$user\">&lt;&lt; $ML{'.back'}</a>";

     my $extrawhere = "";
     if ($filter eq "own" || $filter eq "other") {
         my $op = $filter eq "own" ? "=" : "<>";
         $extrawhere = "AND m.journalid $op $userid";
     }
     
     $sth = $dbr->prepare("SELECT m.memid, u.user, m.journalid, m.jitemid, m.des, m.security ".
                          "FROM memorable m, memkeyword mk, keywords k ".
                          "LEFT JOIN useridmap u ON m.journalid=u.userid ".
                          "WHERE k.keyword=$qkw AND mk.memid=m.memid AND mk.kwid=k.kwid ".
                          "AND m.userid=$userid $secwhere $extrawhere");
     $sth->execute;

     my @memories;
     while (my $mem = $sth->fetchrow_hashref) {
         push @memories, $mem;
     }
     @memories = sort { $a->{$sort} cmp $b->{$sort} } @memories;
    
     $body .= "<ul>\n";
     foreach my $mem (@memories) {
         my $eh_des = LJ::ehtml($mem->{'des'});
         LJ::text_out(\$eh_des);

         my ($entrylink, $editlink);
         if ($mem->{user}) {
             my $itemid = int($mem->{'jitemid'} / 256);
             my $anum = $mem->{'jitemid'} % 256;
             $entrylink = LJ::item_link($mem->{user}, $itemid, $anum);
             $editlink = "/tools/memadd.bml?${authasarg}journal=$mem->{user}&amp;itemid=$mem->{jitemid}";
         } else {
             $entrylink = "/talkread.bml?itemid=$mem->{jitemid}";
             $editlink = "/tools/memadd.bml?${authasarg}itemid=$mem->{jitemid}";
         }

         my $edit = "";
         if ($authasu && $authasu->{user} eq $user) {
             $edit = " [<a href=\"$editlink\">$ML{'.edit'}</a>]";
         }

         my %icons = (
            'friends' => "<?securityprotected?>",
            'private' => "<?securityprivate?>",
         );
         $body .= "<p><li><a href=\"$entrylink\"><b>$eh_des</b></a> $edit $icons{$mem->{security}}<br /><font size='-1'><b>$mem->{'user'}</b></font></li>";
     }
     $body .= "</ul>";
     return;
 }

 $title = $ML{'.title.memorable'};
 $body .= BML::ml(".body.list_categories", { 'user' => $user });

 if ($filter eq "all") {
     $sth = $dbr->prepare("SELECT k.keyword, COUNT(*) AS 'count' FROM memorable m, memkeyword mk, keywords k WHERE mk.memid=m.memid AND mk.kwid=k.kwid AND m.userid=$userid $secwhere GROUP BY k.keyword");
 } else {
     my $op = $filter eq "own" ? "=" : "<>";
     $sth = $dbr->prepare("SELECT k.keyword, COUNT(*) AS 'count' FROM memorable m, memkeyword mk, keywords k WHERE mk.memid=m.memid AND mk.kwid=k.kwid AND m.userid=$userid $secwhere AND m.journalid $op $userid GROUP BY k.keyword");
 }
 $sth->execute;
 my @sortedrows;
 push @sortedrows, $_ while $_ = $sth->fetchrow_hashref;
 @sortedrows = sort { $a->{'keyword'} cmp $b->{'keyword'} } @sortedrows;

 $body .= "<form method='get' action='memories.bml'>";
 $body .= "<input type='hidden' name='user' value=\"$user\" />";
 $body .= "$ML{'.form.filter'} <select name=\"filter\">";
 foreach my $filt ("all", "own", "other") {
     my $sel = $filter eq $filt ? "selected" : "";
     $body .= "<option value=\"$filt\" $sel>$filters{$filt}\n";
 }
 $body .= "</select> <input type='submit' value=\"$ML{'.form.switch'}\">";
 $body .= "</form>";

 unless (@sortedrows) {
     $body .= "<?h1 $ML{'.error.noentries.title'} h1?><?p $ML{'.error.noentries.body'} p?>";
 } else {
     $body .= "<ul>";
     foreach my $row (@sortedrows) {
         my $noun = $row->{'count'}==1 ? $ML{'.entry'} : $ML{'.entries'};
         my $ue_keyword = LJ::eurl($row->{'keyword'});
         my $keyword = $row->{'keyword'};
         LJ::text_out(\$keyword);
         if ($keyword eq "*") { $keyword = $ML{'.uncategorized'}; }
         $body .= "<li><b><a href=\"/tools/memories.bml?user=$user&amp;keyword=$ue_keyword&amp;filter=$filter\">$keyword</a></b>: $row->{'count'} $noun\n";
     }
     $body .= "</ul>";
 }
 return;

_code?><?page
title=><?_code return $title; _code?>
head=><?_code return $head; _code?>
body<=
<?_code return $body; _code?>
<=body
page?><?_c <LJDEP>
link: htdocs/tools/memories.bml, htdocs/tools/memadd.bml, htdocs/talkread.bml
form: htdocs/tools/memories.bml
</LJDEP> _c?>
