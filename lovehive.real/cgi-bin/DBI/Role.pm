package DBI::Role;

use 5.006;
use strict;
use warnings;

our $VERSION = '1.00';

# $self contains:
#
#  DBINFO --- hashref.  keys = scalar roles, one of which must be 'master'.
#             values contain DSN info, and 'role' => { 'role' => weight, 'role2' => weight }
#
#  FROM_DB -- scalar boolean.  get db weights from master db?
#
#  DEFAULT_DB -- scalar string.  default db name if none in DSN hashref in DBINFO
#
#  DBREQCACHE -- cleared by clear_req_cache() on each request.  
#                fdsn -> dbh
#
#  DBCACHE -- role -> fdsn, or
#             fdsn -> dbh
# 
#  DBCACHE_UNTIL -- role -> unixtime
#
#  DB_USED_AT -- fdsn -> unixtime
#
#  DB_DEAD_UNTIL -- fdsn -> unixtime
#
#  NEED_DBWEIGHTS -- scalar boolean.  do we need to reload weights from db?
#
#  MESSAGES_TO -- coderef.  if present, use 'procnotify' table for getting updates,
#                 instead of dbinfo.
#
#  TIME_CHECK -- if true, time between localhost and db are checked every TIME_CHECK
#                seconds
#
#  TIME_REPORT -- coderef to pass dsn and dbtime to after a TIME_CHECK occurence

sub new
{
    my ($class, $args) = @_;
    my $self = {};
    $self->{'DBINFO'} = $args->{'sources'};
    $self->{'TIMEOUT'} = $args->{'timeout'};
    $self->{'FROM_DB'} = $args->{'weights_from_db'};
    $self->{'DEFAULT_DB'} = $args->{'default_db'};
    $self->{'MESSAGES_TO'} = $args->{'messages_to'};
    $self->{'TIME_CHECK'} = $args->{'time_check'};
    $self->{'TIME_LASTCHECK'} = {};  # dsn -> last check time
    $self->{'TIME_REPORT'} = $args->{'time_report'};
    bless $self, ref $class || $class;
    return $self;
}

sub set_sources
{
    my ($self, $newval) = @_;
    $self->{'DBINFO'} = $newval;
    $self;
}

sub clear_req_cache
{
    my $self = shift;
    $self->{'DBREQCACHE'} = {};
}

sub disconnect_all
{
    my $self = shift;
    foreach my $cache (qw(DBREQCACHE DBCACHE)) {
        next unless ref $self->{$cache} eq "HASH";
        foreach my $key (keys %{$self->{$cache}}) {
            my $v = $self->{$cache}->{$key};
            next unless ref $v eq "DBI::db";
            $v->disconnect;
            delete $self->{$cache}->{$key};
        }
    }
    $self->{'DBCACHE'} = {};
    $self->{'DBREQCACHE'} = {};
}

sub same_cached_handle
{
    my $self = shift;
    my ($role_a, $role_b) = @_;
    return 
        defined $self->{'DBCACHE'}->{$role_a} &&
        defined $self->{'DBCACHE'}->{$role_b} &&
        $self->{'DBCACHE'}->{$role_a} eq $self->{'DBCACHE'}->{$role_b};
}

sub flush_cache
{
    my $self = shift;
    foreach (keys %{$self->{'DBCACHE'}}) {
        my $v = $self->{'DBCACHE'}->{$_};
        next unless ref $v;
        $v->disconnect;
    }
    $self->{'DBCACHE'} = {};
    $self->{'DBREQCACHE'} = {};
}

sub trigger_weight_reload
{
    my $self = shift;
    return $self unless $self->{'FROM_DB'};
    $self->{'NEED_DBWEIGHTS'} = 1;
    return $self;
}

sub use_diff_db
{
    my $self = shift;
    my ($role1, $role2) = @_;

    return 0 if $role1 eq $role2;

    # this is implied:  (makes logic below more readable by forcing it)
    $self->{'DBINFO'}->{'master'}->{'role'}->{'master'} = 1;

    foreach (keys %{$self->{'DBINFO'}}) {
        next if /^_/;
        next unless ref $self->{'DBINFO'}->{$_} eq "HASH";
        if ($self->{'DBINFO'}->{$_}->{'role'}->{$role1} &&
            $self->{'DBINFO'}->{$_}->{'role'}->{$role2}) {
            return 0;
        }
    }
    return 1;
}

sub get_dbh
{
    my $self = shift;
    my @roles = @_;
    my $role = shift @roles;
    return undef unless $role;

    my $now = time();

    # if non-master request and we haven't yet hit the master to get
    # the dbinfo, do that first.  (normal code path is something
    # calls LJ::start_request(), then gets master, then gets other)
    # but this path happens also.
    if ($role ne "master" && $self->{'FROM_DB'} &&
        ! $self->{'DBINFO'}->{'_fromdb'})
    {
        # this might be enough to do it, if master isn't loaded:
        $self->{'NEED_DBWEIGHTS'} = 1;
        my $dbh = get_dbh($self, "master");

        # or, if we already had a master cached, we have to
        # load it by hand:
        unless ($self->{'DBINFO'}->{'_fromdb'}) {
            reload_weights($self, $dbh);
        }
    }

    # otherwise, see if we have a role -> full DSN mapping already
    my ($fdsn, $dbh);
    if ($role eq "master") {
        $fdsn = make_dbh_fdsn($self, $self->{'DBINFO'}->{'master'});
    } else {
        if ($self->{'DBCACHE'}->{$role}) {
            $fdsn = $self->{'DBCACHE'}->{$role};
            if ($now > $self->{'DBCACHE_UNTIL'}->{$role}) {
                # this role -> DSN mapping is too old.  invalidate,
                # and while we're at it, clean up any connections we have
                # that are too idle.
                undef $fdsn;

                foreach (keys %{$self->{'DB_USED_AT'}}) {
                    next if $self->{'DB_USED_AT'}->{$_} > $now - 60;
                    delete $self->{'DB_USED_AT'}->{$_};
                    delete $self->{'DBCACHE'}->{$_};
                }
            }
        }
    }

    if ($fdsn) {
        $dbh = get_dbh_conn($self, $fdsn, $role);
        return $dbh if $dbh;
        delete $self->{'DBCACHE'}->{$role};  # guess it was bogus
    }
    return undef if $role eq "master";  # no hope now

    # time to randomly weightedly select one.
    my @applicable;
    my $total_weight;
    foreach (keys %{$self->{'DBINFO'}}) {
        next if /^_/;
        next unless ref $self->{'DBINFO'}->{$_} eq "HASH";
        my $weight = $self->{'DBINFO'}->{$_}->{'role'}->{$role};
        next unless $weight;
        push @applicable, [ $self->{'DBINFO'}->{$_}, $weight ];
        $total_weight += $weight;
    }

    while (@applicable)
    {
        my $rand = rand($total_weight);
        my ($i, $t) = (0, 0);
        for (; $i<@applicable; $i++) {
            $t += $applicable[$i]->[1];
            last if $t > $rand;
        }
        my $fdsn = make_dbh_fdsn($self, $applicable[$i]->[0]);
        $dbh = get_dbh_conn($self, $fdsn);
        if ($dbh) {
            $self->{'DBCACHE'}->{$role} = $fdsn;
            $self->{'DBCACHE_UNTIL'}->{$role} = $now + 5 + int(rand(10));
            return $dbh;
        }

        # otherwise, discard that one.
        $total_weight -= $applicable[$i]->[1];
        splice(@applicable, $i, 1);
    }

    # try others
    return get_dbh($self, @roles);
}

sub make_dbh_fdsn
{
    my $self = shift;
    my $db = shift;   # hashref with DSN info
    return $db->{'_fdsn'} if $db->{'_fdsn'};  # already made?

    my $fdsn = "DBI:mysql";  # join("|",$dsn,$user,$pass) (because no refs as hash keys)
    $db->{'dbname'} ||= $self->{'DEFAULT_DB'} if $self->{'DEFAULT_DB'};
    $fdsn .= ":$db->{'dbname'}";
    $fdsn .= ";host=$db->{'host'}" if $db->{'host'};
    $fdsn .= ";port=$db->{'port'}" if $db->{'port'};
    $fdsn .= ";mysql_socket=$db->{'sock'}" if $db->{'sock'};
    $fdsn .= "|$db->{'user'}|$db->{'pass'}";

    $db->{'_fdsn'} = $fdsn;
    return $fdsn;
}

sub get_dbh_conn
{
    my $self = shift;
    my $fdsn = shift;
    my $role = shift;  # optional.
    my $now = time();

    my $retdb = sub {
        my $db = shift;
        $self->{'DBREQCACHE'}->{$fdsn} = $db;
        $self->{'DB_USED_AT'}->{$fdsn} = $now;
        return $db;
    };

    # have we already created or verified a handle this request for this DSN?
    return $retdb->($self->{'DBREQCACHE'}->{$fdsn})
        if $self->{'DBREQCACHE'}->{$fdsn};

    # check to see if we recently tried to connect to that dead server
    return undef if $self->{'DB_DEAD_UNTIL'}->{$fdsn} && $now < $self->{'DB_DEAD_UNTIL'}->{$fdsn};

    # if not, we'll try to find one we used sometime in this process lifetime
    my $dbh = $self->{'DBCACHE'}->{$fdsn};

    # if it exists, verify it's still alive and return it:
    if ($dbh)
    {
        if ($role && $role eq "master" && $self->{'NEED_DBWEIGHTS'}) {
            return $retdb->($dbh) if reload_weights($self, $dbh);
        } else {
            return $retdb->($dbh) if $dbh->selectrow_array("SELECT CONNECTION_ID()");
        }

        # bogus:
        undef $dbh;
        undef $self->{'DBCACHE'}->{$fdsn};
    }

    # time to make one!
    my ($dsn, $user, $pass) = split(/\|/, $fdsn);
    my $timeout = $self->{'TIMEOUT'} || 2;
    $dsn .= ";mysql_connect_timeout=$timeout";
    $dbh = DBI->connect($dsn, $user, $pass, {
        PrintError => 0,
        AutoCommit => 1,
    });

    # mark server as dead if dead.  won't try to reconnect again for 5 seconds.
    if ($dbh) {
        $self->{'DB_USED_AT'}->{$fdsn} = $now;
        if ($role && $role eq "master" && $self->{'NEED_DBWEIGHTS'}) {
            reload_weights($self, $dbh);
        }
        
        if ($self->{'TIME_CHECK'} && ref $self->{'TIME_REPORT'} eq "CODE") {
            my $now = time();
            $self->{'TIME_LASTCHECK'}->{$dsn} ||= 0;  # avoid warnings
            if ($self->{'TIME_LASTCHECK'}->{$dsn} < $now - $self->{'TIME_CHECK'}) {
                $self->{'TIME_LASTCHECK'}->{$dsn} = $now;
                my $db_time = $dbh->selectrow_array("SELECT UNIX_TIMESTAMP()");
                $self->{'TIME_REPORT'}->($dsn, $db_time, $now);
            }
        }
    } else {
        $self->{'DB_DEAD_UNTIL'}->{$fdsn} = $now + 5;
    }

    return $self->{'DBREQCACHE'}->{$fdsn} = $self->{'DBCACHE'}->{$fdsn} = $dbh;
}

sub reload_weights
{
    my $self = shift;
    my $dbh = shift;

    my $serial;
    my $oldserial = $self->{'CACHE_SERIAL'} || 0;
    my $sql;

    if ($self->{'MESSAGES_TO'}) {
        # we're using the 'procnotify' table, which supports passing
        # arbitrary messages to a callback.  except that we handle the
        # 'DBI::Role::reload' command.
        $sql = "SELECT MAX(nid) FROM procnotify";
    } else {
        # old method, that doesn't use an extra table.
        $sql = "SELECT fdsn AS FROM dbinfo WHERE dbid=0";
    }

    # get new serial number
    $serial = $dbh->selectrow_array($sql);
    return 0 if $dbh->err;

    # do nothing, if serial number hasn't changed
    $self->{'NEED_DBWEIGHTS'} = 0;
    return 1 if $self->{'CACHE_SERIAL'} && $serial == $self->{'CACHE_SERIAL'};
    $self->{'CACHE_SERIAL'} = $serial;

    # in MESSAGES_TO case, we need to load all the messages
    # we missed (unless we just started) and understand them
    # or pass them along to the $MESSAGES_TO coderef
    if ($self->{'MESSAGES_TO'} && $oldserial) {
        # assume the messages are all non-db-related for now:
        my $need_reload = 0;

        my $sql = "SELECT cmd, args FROM procnotify ".
            "WHERE nid > $oldserial AND nid <= $serial";
        my $sth = $dbh->prepare($sql);
        $sth->execute;
        while (my ($cmd, $args) = $sth->fetchrow_array) {
            if ($cmd eq "DBI::Role::reload") {
                $need_reload = 1;
            } else {
                $self->{'MESSAGES_TO'}->($cmd, $args);
            }
        }
        return 1 unless $need_reload || $oldserial == 0;
    }
    
    # if we haven't returned by now, time to reload db weights.
    my $sth = $dbh->prepare("SELECT i.masterid, i.name, i.fdsn, ".
                            "w.role, w.curr FROM dbinfo i, dbweights w ".
                            "WHERE i.dbid=w.dbid");
    $sth->execute;

    my %dbinfo;
    while (my $r = $sth->fetchrow_hashref) {
        my $name = $r->{'masterid'} ? $r->{'name'} : "master";
        $dbinfo{$name}->{'_fdsn'} = $r->{'fdsn'};
        $dbinfo{$name}->{'role'}->{$r->{'role'}} = $r->{'curr'};
        $dbinfo{$name}->{'_totalweight'} += $r->{'curr'};
    }

    # any host that has no total weight (temporarily disabled?), we want
    # to kill all its live connections.
    foreach my $h (keys %dbinfo) {
        my $i = $dbinfo{$h};
        next if $i->{'_totalweight'};

        # kill open connections to it
        delete $self->{'DBCACHE'}->{$i->{'_fdsn'}};

        # mark nothing as wanting to use it.
        foreach my $k (keys %{$self->{'DBCACHE'}}) {
            next if ref $self->{'DBCACHE'}->{$k};
            if ($self->{'DBCACHE'}->{$k} eq $i->{'_fdsn'}) {
                delete $self->{'DBCACHE'}->{$k};
            }
        }
    }

    # copy new config.  good to go!
    $self->{'DBINFO'} = \%dbinfo;
    $self->{'DBINFO'}->{'_fromdb'} = 1;
    1;
}


1;
__END__

=head1 NAME

DBI::Role - Get DBI cached handles by role, with weighting & failover.

=head1 SYNOPSIS

  use DBI::Role;
  my $DBIRole = new DBI::Role {
    'sources' => \%DBINFO,
    'weights_from_db' => 1,       # opt.
    'default_db' => "somedbname", # opt.
  };
  my $dbh = $DBIRole->get_dbh("master");

=head1 DESCRIPTION

To be written.

=head2 EXPORT

None by default.

=head1 AUTHOR

Brad Fitzparick, E<lt>brad@danga.comE<gt>

=head1 SEE ALSO

L<DBI>.

