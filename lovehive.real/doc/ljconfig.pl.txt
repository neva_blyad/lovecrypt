#!/usr/bin/perl
# -*-perl-*-

# LiveJournal configuration file.  Copy this out of the documentation
# directory to cgi-bin/ljconfig.pl and edit as necessary.  The reason
# it's not in the cgi-bin directory already is to protect it from
# getting clobbered when you upgrade to the newest LiveJournal code in
# the future.

# This should be the only file you need to change to get the
# LiveJournal code to run on your site.  If not, it's considered a bug
# and you should report it.

{
    package LJ;

    $HOME = $ENV{'LJHOME'};
    $HTDOCS = "$HOME/htdocs";
    $BIN = "$HOME/bin";
    $TEMP = "$HOME/temp";
    $VAR = "$HOME/var";

    # human readable name of this site as well as shortened versions
    # CHANGE THIS
    $SITENAME = "Some LiveJournal Site";
    $SITENAMESHORT = "YourSite";
    $SITENAMEABBREV = "YS";
    
    ## turn $SERVER_DOWN on while you do any maintenance
    $SERVER_DOWN = 0;
    $SERVER_DOWN_SUBJECT = "Maintenance";
    $SERVER_DOWN_MESSAGE = "$SITENAME is down right now while we upgrade.  It should be up in a few minutes.";

    # the base domain of your site.
    # CHANGE THIS.
    $DOMAIN = "ljsite.com"; 

    # this is what gets prepended to all URLs 
    $SITEROOT = "http://www.$DOMAIN";

    $IMGPREFIX = "$SITEROOT/img";
  
    # set this if you're running an FTP server that mirrors your htdocs/files
    # $FTPPREFIX = "ftp://ftp.$DOMAIN";  

    # path to sendmail and any necessary options
    $SENDMAIL = "/usr/sbin/sendmail -t";

    # command-line to spell checker, or undefined if you don't want spell checking
    # $SPELLER = "/usr/local/bin/ispell -a";
    # $SPELLER = "/usr/local/bin/aspell pipe --sug-mode=fast --ignore-case";

    # where we set the cookies (note the period before the domain)
    # can be one value or an array ref (to accomodate certain old
    # broken browsers)
    $COOKIE_DOMAIN = ["", ".$DOMAIN"]; 
    $COOKIE_PATH   = "/";

    # email addresses
    $ADMIN_EMAIL = "webmaster\@$DOMAIN";
    $SUPPORT_EMAIL = "support\@$DOMAIN";
    $BOGUS_EMAIL = "lj_dontreply\@$DOMAIN";

    # Support URLs of the form http://username.yoursite.com/ ? 
    # If so, what's the part after "username." ?
    $USER_VHOSTS = 1;
    $USER_DOMAIN = $DOMAIN;
  
    # initial friends for new accounts.
    # leave undefined if you don't want to use it.
    # @INITIAL_FRIENDS = qw(news);

    # news site support. if set, that journal loads on the main page.
    #$FRONTPAGE_JOURNAL = "news";

    # Do paid users get email addresses?  username@$USER_DOMAIN ?
    # (requires additional mail system configuration)
    $USER_EMAIL  = 0;

    ## Directory optimizations
    $DIR_DB = "";   # by default, hit the main database (bad for big sites!)

    # DB role to use when connecting to directory DB   
    $DIR_DB_HOST = "master";

    # database info.  only the master is necessary.  
    # you should probably CHANGE THIS
    %DBINFO = (
           'master' => {  # master must be named 'master'
               'host' => "localhost",
               'port' => 3306,
               'user' => 'lj',
               'pass' => 'ljpass',
               'role' => {
                   'cluster1' => 1,
               },
           },
           # example of a TCP-based DB connection
           #'somehostname' => {
           #    'host' => "somehost",
           #    'port' => 1234,
           #    'user' => 'username',
           #    'pass' => 'password',
           #},
           # example of a UNIX domain-socket DB connection
           #'otherhost' => {
           #    'sock' => "$LJ::HOME/var/mysqld.sock",
           #    'user' => 'username',
           #    'pass' => 'password',
           #},
           );

    # List of all clusters - each one needs a 'cluster$i' role in %DBINFO
    @CLUSTERS = (1);    # eg: (1, 2, 3) (for scalability)

    # Which cluster(s) get new users?
    # If it's an arrayref, choose one of the listed clusters at random.
    $DEFAULT_CLUSTER = 1;

    # Can users choose which cluster they're assigned to?  Leave this off.
    $ALLOW_CLUSTER_SELECT = 0; 

    # list of regular expressions matching usernames that people can't have.
    @PROTECTED_USERNAMES = ("^ex_", "^lj_");
    
    # HINTS:
    #   how far you can scroll back on lastn and friends pages.
    #   big performance implications if you make these too high.
    #   also, once you lower them, increasing them won't change anything
    #   until there are new posts numbering the difference you increased 
    #   it by.

    $MAX_HINTS_LASTN = 100;
    $MAX_SCROLLBACK_LASTN = 400;

    # Only turn this on if you are using MySQL replication between
    # multiple databases and have one or more slaves set to not
    # replicated the logtext and talktext tables.  Turning this on
    # makes LJ duplicate all logtext & talktext rows into
    # recent_logtext & recent_talktext which is then replicated.
    # However, a cron job cleans up that table so it's never too big.
    # LJ will try the slaves first, then the master.  This is the best
    # method of scaling your LJ installation, as disk seeks on the
    # database for journal text is the slowest part.
    $USE_RECENT_TABLES = 0;

    # turns these from 0 to 1 to disable parts of the site that are
    # CPU & database intensive or that you simply don't want to use
    %DISABLED = (
                 'interests-findsim' => 0,
                 'directory' => 0,
                 'stats-recentupdates' => 1,
                 'stats-newjournals' => 0,
                 'show-talkleft' => 0,
                 'memories' => 0,
                 'topicdir' => 0,
                 'tellafriend' => 0,
         );

    # require new free acounts to be referred by an existing user?
    # NOTE: new and mostly ljcom-specific.  some features
    # unimplemented in the livejournal-only tree.
    $USE_ACCT_CODES = 0;

    # if you define these, little help bubbles appear next to common
    # widgets to the URL you define:
    %HELPURL = (
        #"accounttype" =>
        #"",
        #"security" => 
        #"noautoformat" => 
        #"userpics" =>
        #"iplogging" =>
        );

    # USER CAPABILITIES CLASSES:
    {
    # default capability class mask for new users: 
    # (16 bit unsigned int ... each bit is capability class flag)
    $NEWUSER_CAPS = 2;

    # default capability limits, used only when no other
    # class-specific limit below matches.
    %CAP_DEF = (
            'maxfriends' => 5,
            'userpics' => 3,
            'checkfriends_interval' => 60,
            'checkfriends' => 1,
            'styles' => 1,
            'makepoll' => 1,
            'useremail' => 0,
            'textmessaging' => 0,
            'todomax' => 25,
            'todosec' => 0,
            'friendsviewupdate' => 30,
            'findsim' => 1,
            'directory' => 1,
            );

    # capability class limits.
    # keys are bit numbers, from 0 .. 15.  values are hashrefs
    # with limit names and values (see doc/capabilities.txt)
    # NOTE: you don't even need to have different capability classes!
    #       all users can be the same if you want, just delete all
    #       this.  the important part then is %CAP_DEF, above.
    %CAP = ();
    }
    
}

1;  # return true
